/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.colval.javase.scrum.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rouanehm
 */
public class Project {

    private int id;
    private String title;
    private List<Release> releases;

    public Project(int id, String title, List<Release> releases) {
        this.id = id;
        this.title = title;
        this.releases = releases;
    }

    public Project(int id, String title) {
        this.id = id;
        this.title = title;
        this.releases = new ArrayList<>();
    }

    public List<Release> getReleases() {
        return releases;
    }

    public void setReleases(List<Release> releases) {
        this.releases = releases;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Project{" + "id=" + id + ", title=" + title + ", releases=" + releases + '}';
    }

}
