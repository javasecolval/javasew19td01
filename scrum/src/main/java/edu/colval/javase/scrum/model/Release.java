/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.colval.javase.scrum.model;

import java.util.List;
import java.util.OptionalDouble;

/**
 *
 * @author rouanehm
 */
public class Release {

    private int id;
    private String version;
    private int expectedVelocity;
    private List<Sprint> sprints;
    private List<Affiliate> team;

    public Release(int id, String version, int expectedVelocity, List<Sprint> sprints, List<Affiliate> team) {
        this.id = id;
        this.version = version;
        this.expectedVelocity = expectedVelocity;
        this.sprints = sprints;
        this.team = team;
    }

    public List<Affiliate> getTeam() {
        return team;
    }

    public void setTeam(List<Affiliate> team) {
        this.team = team;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getExpectedVelocity() {
        return expectedVelocity;
    }

    public void setExpectedVelocity(int expectedVelocity) {
        this.expectedVelocity = expectedVelocity;
    }

    public List<Sprint> getSprints() {
        return sprints;
    }

    public void setSprints(List<Sprint> sprints) {
        this.sprints = sprints;
    }

    @Override
    public String toString() {
        return "Release{" + "id=" + id + ", version=" + version + ", expectedVelocity="
                + expectedVelocity + ", sprints=" + sprints + ", team=" + team + '}';
    }

    public int getStoryPoints() {
        return sprints
                .stream()
                .mapToInt(Sprint::getStoryPoints)
                .sum();
    }

    public OptionalDouble getActualVelocity() {
        return sprints
                .stream()
                .mapToInt(Sprint::getActualVelocity)
                .average();
    }
}
