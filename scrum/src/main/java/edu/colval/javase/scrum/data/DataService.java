/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.colval.javase.scrum.data;

/**
 *
 * @author rouanehm
 */
public class DataService {

    IDAO dao;
    private static DataService single_instance = null;

    public static DataService getInstance() {
        if (single_instance == null) {
            single_instance = new DataService();
        }

        return single_instance;
    }

    private DataService() {
        dao = new  MemoryRepositoryDAO();
    }
    
    public IDAO getRepository() {
        return dao;
    }
}
