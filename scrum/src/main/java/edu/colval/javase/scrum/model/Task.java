/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.colval.javase.scrum.model;

/**
 *
 * @author rouanehm
 */
public class Task {
    private int id;
    private String description;
    private double idealHours;
    private Affiliate affiliate;

    public Task(int id, String description, double idealHours, Affiliate labour) {
        this.id = id;
        this.description = description;
        this.idealHours = idealHours;
        this.affiliate = labour;
    }

    public double getIdealHours() {
        return idealHours;
    }

    public void setIdealHours(double idealHours) {
        this.idealHours = idealHours;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task{" + "id=" + id + ", description=" + description + ", idealHours=" + idealHours + '}';
    }

    public Affiliate getAffiliate() {
        return affiliate;
    }

    public void setAffiliate(Affiliate affiliate) {
        this.affiliate = affiliate;
    }
    
    
}
