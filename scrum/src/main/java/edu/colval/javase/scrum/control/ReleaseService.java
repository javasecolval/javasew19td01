/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.colval.javase.scrum.control;

import edu.colval.javase.scrum.data.DataService;
import edu.colval.javase.scrum.model.Release;
import edu.colval.javase.scrum.model.Sprint;
import edu.colval.javase.scrum.model.Task;
import edu.colval.javase.scrum.model.UserStory;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rouanehm
 */
public class ReleaseService {
    public List<Task> findAllTasksByAffiliateId(int affiliateId) {
        List<Task> foundTasks = new ArrayList();
        List<Release> releases = DataService.getInstance().getRepository().findAllReleases();
        for(Release r : releases)
            for(Sprint s : r.getSprints())
                for(UserStory story : s.getBacklog())
                    for(Task task : story.getTasks())
                        if(task.getAffiliate().getId()==affiliateId)
                            foundTasks.add(task);           
        return foundTasks;
    }
}
