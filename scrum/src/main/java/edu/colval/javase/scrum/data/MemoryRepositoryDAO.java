/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.colval.javase.scrum.data;

import edu.colval.javase.scrum.model.Affiliate;
import edu.colval.javase.scrum.model.Release;
import edu.colval.javase.scrum.model.Sprint;
import edu.colval.javase.scrum.model.UserStory;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import net.andreinc.mockneat.MockNeat;

/**
 *
 * @author rouanehm 
 * https://www.mockneat.com/tutorial/
 */
public class MemoryRepositoryDAO implements IDAO {

    List<Release> releases;

    public MemoryRepositoryDAO() {

        MockNeat mock = MockNeat.threadLocal();

        List<String> roles = Arrays.asList(new String("Coder"),
                new String("Analyst"), new String("Architect"));
        
        List<Affiliate> team = mock.reflect(Affiliate.class)
                .field("id", mock.intSeq().start(1).increment(5))
                .field("name", mock.names().full())
                .field("role", mock.from(roles))
                .list(5)
                .val();

        List<Sprint> sprints = mock.reflect(Sprint.class)
                .field("id", mock.intSeq().start(1).increment(10))
                .field("rank", mock.intSeq().start(1))
                .field("start", mock.localDates())
                .field("end", mock.localDates())
                .field("actualVelocity", mock.ints().range(1, 100))
                .field("backlog", 
                    mock.reflect(UserStory.class)
                        .field("id", mock.intSeq().start(1).increment(20))
                        .field("title", mock.strings().sub(0, 10))
                        .field("points", mock.ints().range(1, 15)) //.toUtilDate()
                        .list(3))
                .list(2)
                .val();
        
        String version = mock.regex("v[.][1-9]{1}[.][1-9]{1}").val();
        this.releases = Arrays.asList(new Release(1, version, 15, sprints, team));

    }

    @Override
    public Optional<Release> findReleaseById(int releaseId) {
        return this.releases.stream().filter(r -> r.getId() == releaseId).findFirst();
    }

    @Override
    public List<Release> findAllReleases() {
        return this.releases;
    }
}
