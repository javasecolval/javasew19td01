/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.colval.javase.scrum.control;

import edu.colval.javase.scrum.data.DataService;
import edu.colval.javase.scrum.model.Affiliate;
import edu.colval.javase.scrum.model.Project;
import edu.colval.javase.scrum.model.Release;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author rouanehm
 */
public class SrumService {
 
    public static void main(String[] args) {
        System.out.println("Agile-Srum v1.0");
        Optional<Release> r = DataService.getInstance().getRepository().findReleaseById(1);
        
        if(r.get()!=null)
            System.out.println(r.toString());
        
        // print only affiliates for given release ...
        r.get().getTeam().stream().forEach(affiliate -> System.out.println(affiliate.toString()));
    }
}
