/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.colval.javase.scrum.model;

import java.util.List;

/**
 *
 * @author rouanehm
 */
public class UserStory {

    private int id;
    private String title;
    private String who;
    private String what;
    private String soThat;
    private int points;
    private String status;
    private List<Task> tasks;
    
    public UserStory() {
    }

    
    public UserStory(int id, String title, String who, String what, String soThat, String status, int points, List<Task> tasks) {
        this.id = id;
        this.title = title;
        this.who = who;
        this.what = what;
        this.soThat = soThat;
        this.status = status;
        this.points = points;
        this.tasks = tasks;
    }

    public UserStory(int id, String title, int points) {
        this.id = id;
        this.title = title;
        this.points = points;
        this.status = new String("Planned");
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getSoThat() {
        return soThat;
    }

    public void setSoThat(String soThat) {
        this.soThat = soThat;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    
    @Override
    public String toString() {
        return "UserStory{" + "id=" + id + ", title=" + title + ", points=" + points + ", status=" + status + '}';
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

}
