/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.colval.javase.scrum.model;

/**
 *
 * @author rouanehm
 */
public class Affiliate {

    private int id;
    private String name;
    private String role;

    public Affiliate(int id, String name, String role) {
        this.id = id;
        this.name = name;
        this.role = role;
    }

    public Affiliate() {
        this.id = id;
        this.name = name;
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Affiliate{" + "id=" + id + ", name=" + name + ", role=" + role + '}';
    }

}
