/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.colval.javase.scrum.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 *
 * @author rouanehm
 */
public class Sprint {

    private int id;
    private int rank;
    //represents a date in ISO format (yyyy-MM-dd) without time.
    private LocalDate start;
    private LocalDate end;
    private int actualVelocity;
    private List<UserStory> backlog;

    public Sprint(int id, int rank, LocalDate start, LocalDate end, int actualVelocity, List<UserStory> backlog) {
        this.id = id;
        this.rank = rank;
        this.start = start;
        this.end = end;
        this.actualVelocity = actualVelocity;
        this.backlog = backlog;
    }

    public Sprint() {
    }

    
    public Sprint(int id, int rank, LocalDate start, LocalDate end) {
        this.id = id;
        this.rank = rank;
        this.start = start;
        this.end = end;
        this.actualVelocity = 0;
        this.backlog = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public int getActualVelocity() {
        return actualVelocity;
    }

    public void setActualVelocity(int actualVelocity) {
        this.actualVelocity = actualVelocity;
    }

    public List<UserStory> getBacklog() {
        return backlog;
    }

    public void setBacklog(List<UserStory> backlog) {
        this.backlog = backlog;
    }

    public int getStoryPoints() {
        return backlog
                .stream()
                .mapToInt(UserStory::getPoints)
                .sum();
    }

    @Override
    public String toString() {
        return "Sprint{" + "id=" + id + ", rank=" + rank + ", start=" + start + ", end=" + end + ", actualVelocity=" + actualVelocity + ", backlog=" + backlog + '}';
    }
    
    
}
